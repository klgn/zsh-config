export DOCKER_MACHINE_NAME="klgn-laptop"
export EDITOR='vim'
export XDG_CONFIG_HOME="/home/klgn/.config"
export LC_ALL=en_GB.UTF-8
export SPACESHIP_CONFIG=$XDG_CONFIG_HOME/spaceship/spaceship.zsh
export FZF_BASE=$HOME/.fzf

export FZF_DEFAULT_OPTS=" \
--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"
