# Display time
SPACESHIP_TIME_SHOW=true

# Display username always
SPACESHIP_USER_SHOW=always

# Do not truncate path in repos
SPACESHIP_DIR_TRUNC_REPO=false

SPACESHIP_USER_PREFIX=
SPACESHIP_USER_SUFFIX=
SPACESHIP_DIR_PREFIX=:
SPACESHIP_GIT_STATUS_STASHED=

SPACESHIP_PROMPT_ORDER=(
  time
  user
  dir
  host
  git
)

# spaceship remove java
# spaceship remove docker
# spaceship remove scala
# spaceship remove venv
